//
//  HPScreenSaverView.h
//  HPScreenSaver
//
//  Created by Mohammed Safwat on 16.10.18.
//  Copyright © 2018 HolidayPirates. All rights reserved.
//

#import <ScreenSaver/ScreenSaver.h>

@interface HPScreenSaverView : ScreenSaverView
@property (nonatomic, assign) NSRect rect;
@property (nonatomic, strong) NSMutableArray<NSImage*> *images;
@property (nonatomic, assign) int currentImageIndex;
@property (nonatomic, strong) NSImageView *imageView;
@property (nonatomic, assign) float animationTime;
@property (nonatomic, assign) int imagesCount;
@end
