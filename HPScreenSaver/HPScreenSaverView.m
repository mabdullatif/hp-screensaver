//
//  HPScreenSaverView.m
//  HPScreenSaver
//
//  Created by Mohammed Safwat on 16.10.18.
//  Copyright © 2018 HolidayPirates. All rights reserved.
//

#import "HPScreenSaverView.h"

@implementation HPScreenSaverView

- (instancetype)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview
{
    self = [super initWithFrame:frame isPreview:isPreview];
    if (self) {
        _currentImageIndex = 0;
        _animationTime = 5.0;
        _imagesCount = 7;
        _imageView = [[NSImageView alloc] initWithFrame:self.frame];
        _images = [[NSMutableArray alloc] init];
        [self setAnimationTimeInterval:_animationTime];
        [self loadImages];
    }
    return self;
}

- (void)startAnimation
{
    [super startAnimation];
}

- (void)stopAnimation
{
    [super stopAnimation];
}

- (void)drawRect:(NSRect)rect
{
    [super drawRect:rect];

    self.rect = rect;

    CGContextRef contextRef = [[NSGraphicsContext currentContext] CGContext];
    CGContextClearRect(contextRef, rect);
    CGContextSetAlpha(contextRef, 1.0);
    NSRectFill(rect);

    if ([_images count] > 0) {
        NSImage *image = _images[_currentImageIndex];

        if (image != nil) {
            [image drawInRect:NSMakeRect(0, 0, self.frame.size.width, self.frame.size.height) fromRect:NSZeroRect operation:NSCompositingOperationScreen fraction:1.0];
        }
    }
}

- (void)animateOneFrame
{
    if ([_images count] > 1 && [_images count] == _imagesCount) {
        [NSAnimationContext runAnimationGroup:^(NSAnimationContext * _Nonnull context) {
            [context setDuration: 0.5];
            [self display];
        } completionHandler:^{
            self.currentImageIndex += 1;
            if (self.currentImageIndex == self.images.count) {
                self.currentImageIndex = 0;
            }
        }];
    }
}

- (BOOL)hasConfigureSheet
{
    return NO;
}

- (NSWindow*)configureSheet
{
    return nil;
}

- (void)loadImages
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];

    for (int i = 1; i < _imagesCount + 1; i++) {
        NSString *imageName = [NSString stringWithFormat:@"%d", i];
        NSImage *image = [bundle imageForResource:imageName];

        if (image != nil) {
            [_images addObject:image];
        }

        if (i == _imagesCount) {
            [self setNeedsDisplay:YES];
        }
    }
}
@end
